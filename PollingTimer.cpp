// Author Jay Bhaumik hello@jaykb.com Released under MIT Open source license https://opensource.org/licenses/MIT
// 
// For usage notes see PollingTimer.h

#include "PollingTimer.h"

//This timer starts running the first time you run PollingTimer.Update()
bool PollingTimer::Update() {

	//Cache current time so we're not repeatedly calling millis()
	unsigned long currentTime = millis();

	if (!_firstTimeRunning)
	{
		//If enough time has passed
		if (millis() - _startTime >= alarmTime)
		{
			Reset();

#ifdef _DEBUG_MSGS_
			Serial.println("polling timer is ready");
#endif // _DEBUG_MSGS_

			//report back that enough time has passed
			return true;
		}
	}
	else
	{
		_firstTimeRunning = false;

#ifdef _DEBUG_MSGS_
		Serial.println("first time this timer is being run");
#endif // _DEBUG_MSGS_

	}

	//report back that timer is not ready
	return false;
}

//Set an alarm time through this function if you didn't set one at construction or if you want to reuse the timer
PollingTimer::SetAlarm(unsigned long alarmTime) {
	this->_alarmTime = alarmTime;
}

PollingTimer::Reset() {
	_firstTimeRunning = 1;
	_startTime = millis();

#ifdef _DEBUG_MSGS_
	Serial.println("polling timer is reset");
#endif // _DEBUG_MSGS_
}

//Constructors
PollingTimer::PollingTimer()

PollingTimer::PollingTimer(unsigned long alarmTime) {
	this->_alarmTime = alarmTime;
}
