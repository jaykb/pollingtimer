#ifndef __POLLINGTIMER__h
#define __POLLINGTIMER__h

// Author Jay Bhaumik hello@jaykb.com Released under MIT Open source license https://opensource.org/licenses/MIT
// DebounceTimer.h

// Usage PollingTimer.h and PollingTimer.cpp should go in your '../Arduino/libraries/PollingTimer/' folder
// Instantiate your timer globally [outside of setup() or loop()], Ex:
// PollingTimer pTimer1 = PollingTimer(1000);

// In your loop() loop call your instantiated timer to run the code, Ex:

// loop(){
//	pTimer1.Update();
// }

// To check if a timer is ready PollingTimer.Update() provides you with a boolean, True for it's ready, Ex:
// loop(){
//	bool canIRunYet = pTimer1.Update();
//  if(canIRunYet){
//      //Yes I can!
//    }
// }

// comment out #define _DEBUG_MSGS_ below to turn off serial debug messages
#define _DEBUG_MSGS_


#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
class PollingTimer {
public:
	PollingTimer();
	PollingTimer(unsigned long alarmTime);
	SetAlarm(unsigned long alarmTime);
	Reset();
	bool Update();

private:
	unsigned long _alarmTime = 100;
	unsigned long _startTime = 0;
	bool _firstTimeRunning = 1;
	
};

#endif
